package com.tsc.smironova.tm.api.repository;

import com.tsc.smironova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getArguments();

    Collection<AbstractCommand> getCommands();

    Collection<String> getCommandName();

    Collection<String> getCommandArgs();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);

}
