package com.tsc.smironova.tm.command.task;

import com.tsc.smironova.tm.api.service.ITaskService;
import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.exception.entity.TaskNotFoundException;
import com.tsc.smironova.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected void showTask(final Task task) {
        if (task == null)
            throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
    }

}
