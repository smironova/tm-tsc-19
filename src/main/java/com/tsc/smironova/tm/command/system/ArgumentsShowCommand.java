package com.tsc.smironova.tm.command.system;

import com.tsc.smironova.tm.command.AbstractCommand;
import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;

import java.util.Collection;

public class ArgumentsShowCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.ARGUMENTS;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.ARGUMENTS;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand arg : arguments) {
            System.out.println(arg.name());
        }
    }

}
