package com.tsc.smironova.tm.command.union;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.TaskNotFoundException;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractProjectTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.TASK_UNBIND_FROM_PROJECT;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.TASK_UNBIND_FROM_PROJECT;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = getProjectTaskService().unbindTaskFromProject(taskId);
        if (task == null)
            throw new TaskNotFoundException();
    }

}
