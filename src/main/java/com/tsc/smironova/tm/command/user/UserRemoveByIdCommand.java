package com.tsc.smironova.tm.command.user;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.UserNotFoundException;
import com.tsc.smironova.tm.exception.user.AccessDeniedException;
import com.tsc.smironova.tm.model.User;
import com.tsc.smironova.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.USER_REMOVE_BY_ID;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.USER_REMOVE_BY_ID;
    }

    @Override
    public void execute() {
        if (getAuthService().isAuth() || getAuthService().isAdmin())
            throw new AccessDeniedException();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final String userId = TerminalUtil.nextLine();
        if (userId.equals(getAuthService().getCurrentUserId()))
            throw new AccessDeniedException();
        final User user = getUserService().removeOneById(userId);
        if (user == null)
            throw new UserNotFoundException();
        showUser(user);
    }

}
