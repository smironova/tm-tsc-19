package com.tsc.smironova.tm.command.user;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.user.AccessDeniedException;

public class UserClearCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.USER_CLEAR;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.USER_CLEAR;
    }

    @Override
    public void execute() {
        if (getAuthService().isAuth() || getAuthService().isAdmin())
            throw new AccessDeniedException();
        System.out.println("[CLEAR USERS]");
        getUserService().clear();
    }

}
