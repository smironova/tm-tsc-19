package com.tsc.smironova.tm.command.project;

import com.tsc.smironova.tm.constant.SystemDescriptionConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.exception.entity.ProjectNotFoundException;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.util.TerminalUtil;

public class ProjectFinishByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConstant.PROJECT_FINISH_BY_INDEX;
    }

    @Override
    public String description() {
        return SystemDescriptionConstant.PROJECT_FINISH_BY_INDEX;
    }

    @Override
    public void execute() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().finishProjectByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
    }

}
