package com.tsc.smironova.tm.util;

import com.tsc.smironova.tm.enumerated.Role;
import com.tsc.smironova.tm.enumerated.Sort;
import com.tsc.smironova.tm.enumerated.Status;
import com.tsc.smironova.tm.model.User;

import java.util.List;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean isEmpty(final List value) {
        return value == null || value.isEmpty();
    }

    static boolean isEmpty(final String[] value) {
        return value == null || value.length == 0;
    }

    static boolean isEmpty(final Integer index) {
        return index == null;
    }

    static boolean isEmpty(final Status value) {
        return value == null;
    }

    static boolean isEmpty(final User value) {
        return value == null;
    }

    static boolean isEmpty(final Role value) {
        return value == null;
    }

    static boolean checkIndex(final Integer index) {
        return index < 0;
    }

    static boolean checkIndex(final Integer index, final int size) {
        return index > size - 1;
    }

    static boolean checkSort(final String value) {
        for (Sort sort : Sort.values()) {
            if (sort.name().equals(value))
                return false;
        }
        return true;
    }

    static boolean checkStatus(final String value) {
        for (Status status : Status.values()) {
            if (status.name().equals(value))
                return false;
        }
        return true;
    }

    static boolean checkRole(final String value) {
        for (Role role : Role.values()) {
            if (role.name().equals(value))
                return false;
        }
        return true;
    }

}
