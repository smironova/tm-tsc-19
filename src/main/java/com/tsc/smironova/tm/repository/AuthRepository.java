package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.api.repository.IAuthRepository;

public class AuthRepository implements IAuthRepository {

    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(final String currentUserId) {
        this.currentUserId = currentUserId;
    }

}
