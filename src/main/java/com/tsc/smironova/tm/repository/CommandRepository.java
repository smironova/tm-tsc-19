package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.api.repository.ICommandRepository;
import com.tsc.smironova.tm.command.AbstractCommand;

import java.util.*;

import static com.tsc.smironova.tm.util.ValidationUtil.isEmpty;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<String> getCommandName() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command: commands.values()) {
            final String name = command.name();
            if (isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public Collection<String> getCommandArgs() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command: commands.values()) {
            final String arg = command.arg();
            if (isEmpty(arg))
                continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(final AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (!isEmpty(arg))
            arguments.put(arg, command);
        if (!isEmpty(name))
            commands.put(name, command);
    }

}
