package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.repository.ITaskRepository;
import com.tsc.smironova.tm.api.service.ITaskService;
import com.tsc.smironova.tm.enumerated.Status;
import com.tsc.smironova.tm.exception.empty.*;
import com.tsc.smironova.tm.exception.system.IndexIncorrectException;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> taskComparator) {
        if (taskComparator == null)
            return null;
        return taskRepository.findAll(taskComparator);
    }

    @Override
    public Task add(final String name, final String description) {
        if (ValidationUtil.isEmpty(name))
            throw new EmptyNameException();
        if (ValidationUtil.isEmpty(description))
            throw new EmptyDescriptionException();
        final Task task = new Task(name, description);
        add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null)
            return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null)
            return;
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) {
        if (ValidationUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (ValidationUtil.isEmpty(index))
            throw new EmptyIndexException();
        else if (ValidationUtil.checkIndex(index))
            throw new IndexIncorrectException(index + 1);
        else if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name) {
        if (ValidationUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneById(final String id) {
        if (ValidationUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        if (ValidationUtil.isEmpty(index))
            throw new EmptyIndexException();
        else if (ValidationUtil.checkIndex(index))
            throw new IndexIncorrectException(index + 1);
        else if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneByName(final String name) {
        if (ValidationUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (ValidationUtil.isEmpty(id))
            throw new EmptyIdException();
        if (ValidationUtil.isEmpty(name))
            throw new EmptyNameException();
        if (ValidationUtil.isEmpty(description))
            throw new EmptyDescriptionException();
        final Task task = findOneById(id);
        if (task == null)
            return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (ValidationUtil.isEmpty(index))
            throw new EmptyIndexException();
        else if (ValidationUtil.checkIndex(index))
            throw new IndexIncorrectException(index + 1);
        else if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        if (ValidationUtil.isEmpty(name))
            throw new EmptyNameException();
        if (ValidationUtil.isEmpty(description))
            throw new EmptyDescriptionException();
        final Task task = findOneByIndex(index);
        if (task == null)
            return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id) {
        if (ValidationUtil.isEmpty(id))
            throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        if (ValidationUtil.isEmpty(index))
            throw new EmptyIndexException();
        else if (ValidationUtil.checkIndex(index))
            throw new IndexIncorrectException(index + 1);
        else if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        final Task task = findOneByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) {
        if (ValidationUtil.isEmpty(name))
            throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String id) {
        if (ValidationUtil.isEmpty(id))
            throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) {
        if (ValidationUtil.isEmpty(index))
            throw new EmptyIndexException();
        else if (ValidationUtil.checkIndex(index))
            throw new IndexIncorrectException(index + 1);
        else if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        final Task task = findOneByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) {
        if (ValidationUtil.isEmpty(name))
            throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (ValidationUtil.isEmpty(id))
            throw new EmptyNameException();
        if (ValidationUtil.isEmpty(status))
            throw new EmptyStatusException();
        final Task task = findOneById(id);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (ValidationUtil.isEmpty(index))
            throw new EmptyIndexException();
        else if (ValidationUtil.checkIndex(index))
            throw new IndexIncorrectException(index + 1);
        else if (ValidationUtil.checkIndex(index, taskRepository.size()))
            return null;
        if (ValidationUtil.isEmpty(status))
            throw new EmptyStatusException();
        final Task task = findOneByIndex(index);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final String name, final Status status) {
        if (ValidationUtil.isEmpty(name))
            throw new EmptyNameException();
        if (ValidationUtil.isEmpty(status))
            throw new EmptyStatusException();
        final Task task = findOneByName(name);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

}
