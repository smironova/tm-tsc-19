package com.tsc.smironova.tm.exception.entity;

import com.tsc.smironova.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project was not found...");
    }

}
