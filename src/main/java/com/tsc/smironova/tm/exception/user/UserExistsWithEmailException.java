package com.tsc.smironova.tm.exception.user;

import com.tsc.smironova.tm.exception.AbstractException;

public class UserExistsWithEmailException extends AbstractException {

    public UserExistsWithEmailException(final String email) {
        super("Error! Such email ``" + email + "`` is already used...");
    }

}
