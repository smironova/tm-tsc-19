package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyTaskIdException extends AbstractException {

    public EmptyTaskIdException() {
        super("Error! Task ID is empty...");
    }

}
