package com.tsc.smironova.tm.exception;

public class AbstractException extends RuntimeException {

    public AbstractException() {
        super();
    }

    public AbstractException(final String message) {
        super(message);
    }

    public AbstractException(final Throwable cause) {
        super(cause);
    }

}
