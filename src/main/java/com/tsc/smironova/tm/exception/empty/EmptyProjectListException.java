package com.tsc.smironova.tm.exception.empty;

import com.tsc.smironova.tm.exception.AbstractException;

public class EmptyProjectListException extends AbstractException {

    public EmptyProjectListException() {
        super("No projects! Add new project...");
    }

}
